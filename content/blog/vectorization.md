---
title: "Vectorization"
date: 2018-08-26
tags : [ "software" ]
draft: true
---

"new" trend in software development
instead of high-level loops: pass list-like data stuctures through to lower/faster levels

vectorization in CPUs (SIMD, AVX512)
    Fortran can do it, features in newer languages, autovectorization

in protocols
    pipelining in http

graphql instead of rest
    single call, give full list and subdependencies

ansible
    outer loops vs. lists in modules like apt/zypper/pip
