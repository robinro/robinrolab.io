---
title: Aggregation
date: 2021-01-04
---

Aggregation of information is super powerful. Be it news sites aggregating different articles
and categories based on your interests or locations or a personally-aggregated list of RSS feeds
or subscribed newsletters.

<!--more-->

## Why

There's way to much information out there to consume all of it.
Reading primary sources is only possible in a very narrow area.

## How

### Let the crowd decide

[Hacker Newsletter](https://hackernewsletter.com/) lists the top voted articles on [Hacker News](https://news.ycombinator.com/). Hacker News itself has an enormous amount of content flowing through and the newsletter is a quick way to catch up without spending much time.

### Let an expert decide

Several people share what they read and find interesting in aggregate/summarized form.
This format gives a prefiltered/annotated list of more low-level sources.
These can take the form of weekly newsletters (e.g.
[Recomendo](https://www.recomendo.com/)), daily blogs (e.g. [Fefe](https://blog.fefe.de/))
or even yearly lists (e.g. [this one](https://medium.com/fluxx-studio-notes/52-things-i-learned-in-2020-6a380692dbb8)).
