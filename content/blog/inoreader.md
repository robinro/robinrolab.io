---
title: "Inoreader"
date: 2019-06-22T18:39:59+02:00
lastmod: 2019-06-22T18:39:59+02:00
tags : ["reading", "inoreader"]
categories : [ "reading" ]
layout: post
type:  "post"
highlight: false
draft: true
---

## Keeping up to date

When working in the IT sector, it is important to **stay up to date** with new technologies.
The only constant is change, so one better keeps up with it.
There is a myriad of ways to stay informed with new things and it depends a lot on personal
taste and requirements to choose.

Currently I rely a lot on **RSS feeds** with a few **newsletters** in addition.

## Feeds
There was a hype around blogs and blogging in the late 2000s.
The blogosphere not only relies on feeds to track new posts, but has features like
backreferences TODO?
to link from a source to places mentioning it.
One of the main propierties of blogs and feeds is the distributed and standardized nature.
It is possible to read blogs by different authors using different hosting providers and
blogging software in the same reader.
This stands in contrast to most modern social networks and even platforms like medium, which
try to keep users on their own site and deliberately limit interoperability.

While it's possible to run a local feed reader and scrape feeds directly, it is more
convenient to use a cloud solution.
The main driver for me it the possibility to use multiple devices and keep feeds and
read/unread state in sync, also while working offline.

When I first moved from a local reader to the cloud I used Google Reader, which was
discontinued and had a vivid user base complaining about it.
The majority of these users, including me, moved to Feedly.
Feedly early offered a paid version with additional features, but after some time cut the
free version back to (today) 3 feeds. TODO?
This brought me to Inoreader, which I currently use.
TODO
    Free/Paid
    Social features
    Link to public feed

## Newsletters
TODO
    Hackernews

