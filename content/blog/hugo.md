---
title: Hugo
date: 2018-08-14
---

This page is now using [hugo](https://github.com/gohugoio/hugo/) with the [hyde-hyde
theme](https://themes.gohugo.io/hyde-hyde/) and is hosted on Gitlab based on the [Gitlab
Pages example](https://gitlab.com/pages/hugo).
<!--more-->
