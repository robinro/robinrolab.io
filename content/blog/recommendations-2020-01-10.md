---
title: Recommendations
date: 2020-01-19
---
This periodic category lists recommendations, mostly for blogs and podcasts.

## [WDR Zeitzeichen](https://www1.wdr.de/mediathek/audio/zeitzeichen/index.html)

This daily podcast 
([feed](https://www1.wdr.de/mediathek/audio/zeitzeichen/zeitzeichen-podcast-100.podcast))
discusses historic events, usually motivated by anniversaries.

It's well produced and the content is a nice mix of old and more recent events.
Some of the latest episodes I listened to:

* Das Betriebsrätegesetz wird beschlossen (am 18.1.1920)
* Biafrakrieg endet (am 15.1.1970)
* Ersatzdienstgesetz wird verkündet (am 13.1.1960)
* Gründung der Partei "Die Grünen" (am 12.1.1980)

## [HackerNewsletter](https://hackernewsletter.com/)

This newsletter contains a weekly summary of the top stories on 
[Hacker News](https://news.ycombinator.com/).
It allows to keep in the loop without checking the page every day.
The layout is also much more reader-friendly than the standard one on hackernews.
<!--more-->
