---
title: "Podcast Recommendation: Planet Money"
date: 2020-02-23T16:06:39+01:00
---

Planet Money is a podcast discussing different topics in economics ([feed](https://www.npr.org/rss/podcast.php?id=510289)).
Besides being very informative, they authors add an entertaining spin to make this a recreational topic.

In a recent episode, which I'd suggest as a good entry point for this podcast is,
multiple famous economists are asked for their opinion on
[What's the most useful idea in economics?](https://www.npr.org/2020/01/09/794977811/episode-963-13-000-economists-1-question?t=1582470496397).
This leads to discussions on the basics of economic theory and historic developments.

<!--more-->
