---
draft: true
title: Living in the Terminal
date: 2018-08-31
tags: ["software", "cli"]
---

A collection of modern alternatives to some of the most basic tools can be found at https://remysharp.com/2018/08/23/cli-improved.

Some tools are so relevant, that I will always use them instead of the original:
* [htop](http://hisham.hm/htop/): Replaces top, has colorful output and a lot more features
    including cursor navigation, search, highlighing. Useful shortcuts: `P` to sort
    processes by CPU usage, `M` to sort by memory usage.
* [FZF](https://github.com/junegunn/fzf): Fuzzyfinder, most useful together with the zsh
    plugin to replace history search (`Ctrl-R`) and provider file name search (`Ctrl-T`).
* [fd](https://github.com/sharkdp/fd/): Replaces find. Easier syntax (`fd foo` instead of
    `find -iname "*foo*"`) and better defaults (respect gitignore, ignore binary files).

Things that I didn't use before reading the article but now included in my setup:
* [prettyping](https://github.com/denilsonsa/prettyping): Replace ping with colorful and
    more concise output
* 
