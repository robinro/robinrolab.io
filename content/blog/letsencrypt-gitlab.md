---
title: Using Let's Encrypt with Gitlab Pages
date: 2018-08-15
---

This page uses SSL certificates by Let's Encrypt. Getting those into Gitlab Pages requires
some effort. The general idea is described in [this post](https://autonomic.guru/lets-encrypt-gitlab-again/). For details checkout the code in the [public repo for this page](https://gitlab.com/robinro/robinro.gitlab.io/tree/master/certbot).
<!--more-->
