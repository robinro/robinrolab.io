---
title: About me
comments: false
type: about
---

My name is Robin Roth and I'm working as a Site Reliability Engineer at [Google](https://landing.google.com/sre/).

Previously I was a Software Consultant at [TNG](https://www.tngtech.com), designing systems
based on Kubernetes and running them on AWS.
Before working full-time in the software industry I graduated with a PhD
([thesis](https://publikationen.bibliothek.kit.edu/1000071496)) in
[Theoretical Particle Physics](https://www.itp.kit.edu/) from 
[KIT](https://www.kit.edu), Karlsruhe.
