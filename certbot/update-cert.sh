#!/bin/bash
set -e
set -o pipefail
cd $(dirname $0)
PATH=$PATH:${PWD}
#certbot certonly --config config.ini --manual-auth-hook ./gitlab-auth-webhook.sh --manual --preferred-challenges=http -n --manual-public-ip-logging-ok
certbot renew --config config.ini

echo "success generating"
echo "uploading certs to gitlab"
for domain in rroth.de www.rroth.de; do
    CERT_PEM=$(cat config/live/${domain}/fullchain.pem)
    KEY_PEM=$(cat config/live/${domain}/privkey.pem)
    curl --silent --request PUT --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" --form "certificate=$CERT_PEM" --form "key=$KEY_PEM" https://gitlab.com/api/v4/projects/robinro%2Frobinro.gitlab.io/pages/domains/${domain} | jq 'del(.certificate.certificate) | del(.certificate.certificate_text)'
done
