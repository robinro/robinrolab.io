#!/bin/bash

## gitlab-auth-hook.sh
## Certbot Manual Authentication routine that pushes nounce files
## into a Gitlab repo and waits until Gitlab CI/CD pipeline publishes file
## before exiting to allow Certbot to continue authentication.
##
## Usage: Script is called by certbot and is provided as a command line argument.
## Example:
## certbot certonly --config autonomic-guru.ini --manual-auth-hook gitlab-auth-hook.sh \
##               --manual --preferred-challenges=http -n --manual-public-ip-logging-ok
##

## Where the Nounce files live in your repo
SaveDir="../acme-challenge"
## URI path appended to domain that script uses to check publishing
CertPath="/.well-known/acme-challenge/"

## Nothing past here should probably need adjusting...
## create the file, add it to git, commit it, and push it.
cd ${SaveDir}
echo ${CERTBOT_VALIDATION} > ${CERTBOT_TOKEN}
git add -- ${CERTBOT_TOKEN}
git commit -m "certbot token" -- ${CERTBOT_TOKEN}
git push

## Check and wait for Nounce to be published.
until wget -q --no-check-certificate "http://${CERTBOT_DOMAIN}${CertPath}${CERTBOT_TOKEN}"
do 
    sleep 15
done

